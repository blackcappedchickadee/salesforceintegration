# DEV 502 #

DEV 502 Bits.

### Streaming API ###

This code sample - Anonymous Apex that can be placed in either the Developer Console or in Workbench->Utilities->Apex Execute - creates a new PushTopic named openPriorityPositions. Only Create and Update events on the Position__c custom sObject - where Priority__c is Critical or High - will fire off this "push" to any listening "subscribers". Refer to DEV502, Exercise 801.


			PushTopic myPT = new PushTopic();
			myPT.Name='openPriorityPositions';
			myPT.Query = 'SELECT Id, Name, Status__c FROM Position__c WHERE Status__c = \'Open\' AND Priority__c IN ( \'Critical\', \'High\') ';
			myPT.ApiVersion = 32.0;
			myPT.NotifyForOperationCreate = true;
			myPT.NotifyForOperationUpdate = true;
			myPT.NotifyForOperationDelete = false;
			myPT.NotifyForOperationUndelete = false;
			myPT.NotifyForFields = 'Where';
			insert myPT;
